#!/bin/sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=mb
#SBATCH --job-name=mem
#SBATCH --error=mem-%j.err
#SBATCH --output=mem-%j.out
#SBATCH --workdir=/home/fzyulkyarov/MemoryReliability
#SBATCH --time=00:05:00

./MemoryReliability -m 100 -o test.1.log -i 10000 -h 100 -s 2
