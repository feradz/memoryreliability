#ifndef _DEFINES_H
#define _DEFINES_H

typedef uintptr_t ADDRVALUE;
typedef void* ADDRESS;

const unsigned int KILO = 1024;
const unsigned int MEGA = 1048576;
const unsigned int GIGA = 1073741824;

#endif
