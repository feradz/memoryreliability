#!/bin/bash

if [ "$1" = "" ]; then
    echo "$0 <directory name with job files>"
    exit 1
fi

FILES=$1/*.sh

for f in $FILES;
do
	echo "Submitting $f"
	sbatch $f
done

