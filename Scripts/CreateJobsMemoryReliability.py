#!/usr/bin/python
'''
Created on December 29, 2014

This script creates SLURM job files for the MemoryReliability program.

@author: Ferad Zyulkyarov
@email: ferad.zyulkyarov@bsc.es
'''

import sys

def printUsage():
    ''' Prints the usage
    '''
    print "Usage: " + sys.argv[0] + " <numJobs>"

def generateJobFile(ind):
    ''' Generate a job file with the specified index.
    '''
    lines  = "#!/bin/sh\n"
    lines += "#SBATCH --ntasks=1\n"
    lines += "#SBATCH --cpus-per-task=2\n"
    lines += "#SBATCH --partition=mb-benchmark\n"
    lines += "#SBATCH --job-name=fzm" + str(ind) + "\n"
    lines += "#SBATCH --error=mem-" + str(ind) + "-%j.err\n"
    lines += "#SBATCH --output=mem-" + str(ind) +"-%j.out\n"
    lines += "#SBATCH --workdir=/home/fzyulkyarov/MemoryReliability\n"
    lines += "#SBATCH --time 00:05:00\n"
    lines += "./MemoryReliability -m 2800 -o mem-" +str(ind) + ".log -s 10\n"
    
    fileName = "job-" + str(ind) + ".sh"
    f = open(fileName, "w")
    f.write(lines)
    f.close();
    
    print "Created job file: " + fileName
    
def main():
    ''' The main entry point method to this script
    '''
    argc = len(sys.argv)
    if argc != 2:
        printUsage()
        return 0
    
    numJobs = int(sys.argv[1])
    for i in range(numJobs):
        generateJobFile(i)
    
    print "DONE"
        
    


if __name__ == '__main__':
    main()